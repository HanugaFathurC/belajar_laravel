<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast; //Untuk mengarahkan ke model Cast

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all(); //tidak perlu kasih App karena sudah dilakukan use

        return view('cast.index', compact('cast')) ;


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all()); //Untuk testing apakah datanya masuk atau tidak
        $validatedData = $request->validate([
            'nama' => 'required| max:255',
            'umur' => 'required',
            'bio' => 'required'

        ],
        [
            'nama.required' => 'Nama wajib diisi !',
            'umur.required'  => 'Isilah umur pemain !',
            'bio.required'  => 'Harapan isikan biodata pemain !'

        ]
            
        );
        $cast = new Cast;
    
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;



        $cast->save();
        return redirect('/cast'); //Untuk langsung mengarahkan ke path
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id)->first(); //first digunakan agar tidak looping karena hanya dibutuhkan satu data saja

        return view('cast.show', compact('cast')) ; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id); //first digunakan agar tidak looping karena hanya dibutuhkan satu data saja

        return view('cast.edit', compact('cast')) ; 
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required| max:255',
            'umur' => 'required',
            'bio' => 'required'

        ],
        [
            'nama.required' => 'Nama wajib diisi !',
            'umur.required'  => 'Isilah umur pemain !',
            'bio.required'  => 'Harapan isikan biodata pemain !'

        ]
            
        );

        $cast = Cast::find($id);
 
        $cast->nama = $request['nama'] ;
        $cast->umur = $request['umur'] ;
        $cast->bio = $request['bio'] ;
        
        $cast->save();

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
 
        $cast->delete();

        return redirect('/cast') ;
    }
}
