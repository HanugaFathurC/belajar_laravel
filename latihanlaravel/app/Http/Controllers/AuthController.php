<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('register') ;
    }

    public function signup(Request $request){
        // Cara Cek Data Sudah berhasil di transfer atau belum dd($request->all()) ;
        $first_name = $request['first-name'];
        $last_name = $request['last-name'];
        return view('welcome', compact('first_name','last_name' ));
    }
}
