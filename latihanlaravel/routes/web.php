<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index' );

Route::get('/register', 'AuthController@regis');

Route::post('/welcome', 'AuthController@signup');

Route::get('/data-table', 'IndexController@table');

//CRUD Cast

//Create
Route::get('/cast/create', 'CastController@create'); //arah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table 

//Read
Route::get('/cast', 'CastController@index' ); //ambil data ke database lalu ditampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show') ; //reoute detail castnya

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route untuk mengarah ke form edit
Route::get('/cast/{cast_id}', 'CastController@update'); //Route untuk menyimpan perubahan 

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy') ; //Route untuk delete
