@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('content')
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
  @csrf
  <p>First Name :</p>
  <input type="text" name="first-name">
  <br>
  <p>Last Name :</p>
  <input type="text" name="last-name">
  <br>
  <br>
  <p>Gender</p>
  <input type="radio" name="gender" value="male">Male <br>
  <input type="radio" name="gender" value="female">Female <br>
  <input type="radio" name="gender" value="others">Others <br>
  <br>
  <p>Nationality</p>
  <select>
    <option name="nationality" value="Ina">Indonesia</option>
    <option name="nationality"  value="Jpn">Jepang</option>
    <option name="nationality"  value="Bld">Belanda</option>
  </select>
  <br>
  <br>
  <p>Language Spoken</p>
  <input type="checkbox" name="language" value="bahasa_ina">
  <label for="language1">Bahasa Indonesia</label>
  <br>
  <input type="checkbox" name="language" value="english">
  <label for="language2">English</label>
  <br>
  <input type="checkbox" name="language" value="other">
  <label for="other">Other</label>
  <br>
  <br>
  <p>Bio</p>
  <textarea name="bio" cols="30" rows="10">
    
  </textarea>
  <br>
  <input type="submit" value="Sign Up" >
  
</form>
@endsection

  
  
