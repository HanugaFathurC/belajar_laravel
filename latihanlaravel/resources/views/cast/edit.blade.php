@extends('layout.master')
@section('title')
Halaman Edit Pemain 
@endsection
@section('content')

<form method="PUT" action="/cast/{{ $cast->id }}">
  @csrf
  <div class="form-group">
    <label>Nama Pemain</label>
    <input type="text" name="nama" value="{{ $cast -> nama }}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="number" name="umur" value = "{{  $cast -> umur }}" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio"  cols="30" rows="10" class="form-control">{{ $cast -> bio }}</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection